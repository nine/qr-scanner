package main

import (
	"flag"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"

	// import gozxing
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
)

var (
	filePath *string
	verbose  *bool
)

func init() {
	// initialise flags
	verbose = flag.Bool("v", false, "Enable verbose mode")
}

func FatalError(err error) {
	/* if in verbose mode, log fatal error;
	 * if not in verbose mode, print empty string
	 * and exit
	 */
	if err != nil {
		if *verbose {
			log.Fatal(err)
		} else {
			fmt.Println("")
			os.Exit(0) // exit without any message
		}
	}
}

func ParseFilePath() {
	// grab file path from the last arg
	args := os.Args
	filePath = &args[len(args)-1]
}

func main() {
	flag.Parse()
	ParseFilePath()

	file, err := os.Open(*filePath)
	FatalError(err)

	img, _, err := image.Decode(file)
	// image.Decode() returns image, image type as string, and error
	FatalError(err)

	bmp, err := gozxing.NewBinaryBitmapFromImage(img)
	FatalError(err)

	qrReader := qrcode.NewQRCodeReader()
	result, err := qrReader.Decode(bmp, nil)

	if result == nil {
		if *verbose {
			fmt.Println("INFO: Image processed, no QR code found.")
		} else {
			fmt.Println("")
		}
		return
	}
	fmt.Println(result)
}
