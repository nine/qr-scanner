Command-line utility to read QR codes from `jpeg/jpg` and `png` images.

Usage: `scan [OPTIONS] /path/to/file`

**Relevant Links**
- [Installing dependencies from `go.mod`](https://stackoverflow.com/a/69474718)
- [Adding Go dependencies](https://go.dev/doc/modules/managing-dependencies#adding_dependency)
- [A lot of information on how QR codes are made](https://www.nayuki.io/page/creating-a-qr-code-step-by-step)
- [gozxing](https://stackoverflow.com/a/69474718) 
